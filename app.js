const express = require("express");
const path = require("path");
const app = express();

require("dotenv").config();

const publicPath = path.join(__dirname, "public")
let oldFile = '<h1>Hi</h1>';
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(express.static(publicPath));


app.get("/", (req, res) => {
    res.sendFile(path.join(publicPath, "html", "index.html"))
})

app.post("/userSubmit", (req, res) => {
    console.log(req.body);
    oldFile = req.body.user_input;
    res.sendFile(path.join(publicPath, "html", "index2.html"))
});

app.get("/oldData", (req, res) => {
    res.json(oldFile);
})

const PORT = process.env.PORT;
const server = app.listen(PORT, () => {
    const ADDRESS = process.env.NODE_ENV === "production" ? server.address().address : "localhost";
    const MYPORT = process.env.NODE_ENV === "production" ? server.address().port : PORT;
    console.log(`Server running at http://${ADDRESS}:${MYPORT}`);
});