tinymce.init({
    selector: "#tinyMceRoot",
})

const getDataBtn = document.getElementById("oldData") || null;
const viewBtn = document.getElementById("view") || null;
const viewContainer = document.getElementById("viewData") || null;

if(getDataBtn){
    getDataBtn.addEventListener("click",async () => {
        const response = await fetch("/oldData");
        const data = await response.json();
        console.log(data)
        tinymce.activeEditor.setContent(data);
    })
}

viewBtn.addEventListener("click", () => {
    viewContainer.innerHTML = tinymce.activeEditor.getContent();
})